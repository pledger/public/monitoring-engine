# MONITORING ENGINE

## DESCRIPTION
This component will manage the store and retrieve of metric values from different infrastructure resources and app/services in a central time series database to allow other component/subsystem (consumers) to query for specific metrics in a specific time interval. This metric database will be centralized in the Pledger hub cluster and with the help of some external tools/exporters/extractors/producers will gather metrics samples in a periodic interval and will send them to the central data-store.

The main functionality of the component remains as a central point of service to store and retrieve metrics requested or needed for the users of  Pledger. We also extend the functionality to include a new ETL module or subcomponent to extract, transform and load the telemetry samples of the edge or remote infrastructures and applications in the central metric data store of Pledger. These metrics samples or scrapes will be sent by the infrastructure owners or applications owners via the StreamHandler component of Pledger to the Pledger hub (central) cluster. In this way, we avoid the usual security constraints at the edge that do not allow us to open inbound connections by default.

The Monitoring Engine component related to technical management and control of store and retrieve metric data will be implemented as two containerized applications with the following modules or microservices:
-	Monitoring Engine (manager): this module manages the query interface to serve request from consumers of metrics acting as a single point of service and will also implement the main business logic to find the current placement/infrastructure of an application and it associated metric data store. This component implements a REST API to allow other components of  Pledger to query for some metrics values from metric data stores.
-   Kafka to Prometheus (ETL): this module implements the ETL process (extract, transform, load) for the feeding of remote or edge metrics (system or application metrics) to the  Pledger central metric data store.


## REQUIREMENTS
The following software components and credentials are pre-requirements to install the Monitoring Engine component:
-	Kubernetes cluster ver. 1.20+ (this cluster will be the hub cluster)
-	Credentials or service account in the hub cluster with permission to deploy applications.
-	StreamHandler client certificate credentials as secrets object in K8S cluster.
-	Endpoint configuration of the StreamHandler component.
-	The two container images of the component in a container image repository.
-	Prometheus Operator installed in the K8S hub cluster.

## INSTALLATION
The first step is to download the source code from the Pledger Gitlab repository:

```
    git clone  https://gitlab.com/pledger/public/monitoring-engine.git
```

to the next step is to build the docker image of the manager and the ETL components:

```
    cd monitoring-engine/openapi_server
    docker build --rm -t monitoring-engine .
    cd ../k2p-exporter
    docker build --rm -t k2p .
    cd ..
```

To install the Monitoring Engine components, the command line tool kubectl from K8S is required. It is also possible to install this subsystem in the testbed of the project using the project’s CI/CD tools (Jenkins pipelines).

To customize the installation to a specific environment, the “kustomizer” tool may be used along with the corresponding yaml files. Some examples of this customization can be seen in the subdirectory “overlays” of the project. For example, the command to install the manager component in the testbed is the following:

```
    cd openapi_server
    kubectl apply -k overlays/testbed 
```

For more details of manifest files (yaml) can be found in:
-	https://gitlab.com/pledger/public/monitoring-engine/openapi_server/-/tree/master/base and
-	https://gitlab.com/pledger/public/monitoring-engine/openapi_server/-/tree/master/overlays/testbed.

There are several environment variables to configure before installing the subsystem. These variables are declared in the manifest files of the subsystem (yaml files). 

| Name | Description | Default | Mandatory | Example |
| ---  | --- | --- | --- | --- |
| ME_E2CO_ENDPOINT (for the manager) | URL of the Orchestrator component REST API | | Yes | http://e2co-app:8333 |
| ME_DEFAULT_PROM (for the manager)	| Default Prometheus server endpoint. This is the centralized PLEDGER hub Prometheus system. | | Yes | http://kube-prometheus-stack-prometheus.monitoring:9090 |
| K2P_KAFKA_CLIENT (for the ETL) | Kafka client implementation | | No | kafka2 |
| K2P_KAFKA_ENDPOINT (for the ETL) | Kafka brokers addresses | | No | - |
| K2P_KAFKA_METRIC_TOPIC (for the ETL) | Kafka topic for consuming sla violation alerts | | No | metrics |
| K2P_KAFKA_KEYSTORE_PASSWORD (for the ETL) |	Kafka KeyStore password for client certificates | | No | password |
| K2P_KAFKA_KEY_PASSWORD (for the ETL) |	    Kafka private key password for client certificates | | No | password |
| K2P_KAFKA_TRUSTSTORE_PASSWORD (for the ETL) |	Kafka truststore password for server public certificates (CA) | | No | password |
| K2P_KAFKA_KEYSTORE_LOCATION (for the ETL) |	Kafka KeyStore location in container filesystem | | No | /var/kafka_keystore/kafka.client.keystore.p12 |
| K2P_KAFKA_TRUSTSTORE_LOCATION (for the ETL) |	Kafka truststore location in container filesystem | | No | /var/kafka_truststore/kafka.client.truststore.pem |
| K2P_SCRAPE_INTERVAL (for the ETL) | Time in seconds of the polling frequency from Prometheus to the ETL module to collect new metric samples. | | No | 60s |
| K2P_PROXY_NAMESPACE (for the ETL) | Kubernetes namespace where the ETL module is deployed. | | No | core |
| K2P_PROXY_SERMONSEL (for the ETL) | Key value used by Prometheus Operator to auto-discover new services/applications to monitor | | Yes | release: kube-prometheus-stack |

## USAGE
We can query for metric values using the REST API of the manager module. For the list of API calls and testing them you can use the following URL for the swagger UI: http://localhost:4040/swaggerui. You might need to replace the host:port part of the URL with your IP and port. We can also use a command line tool like curl to test the API. For example, the next line returns the last metric value for the metric “up” (availability) of the application with ID = 1.

```
    curl -X GET "http://localhost:4040/api/v1/query?query=up&appId=1" -H "accept: application/json".
```

## LICENSES
Monitoring Engine component is licensed under Apache license version 2.

| ![EU Flag](http://www.consilium.europa.eu/images/img_flag-eu.gif) | This work has received funding by the European Commission under grant agreement No. 871536, Pledger project. |
|---|--------------------------------------------------------------------------------------------------------|

