//
// Copyright 2021 Atos
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 02 Nov 2021
// Updated on 02 Nov 2021
//
// @author: ATOS
//

package templates

import (
	"bytes"
	"html/template"

	log "atos.pledger/k2p/common/logs"
)

// path used in logs
const pathLOG string = "K2P > Templates "

func NewServiceTemplate(service interface{}, headless bool) string {
	var template_path string = "./templates/Service.tmpl"
	if headless {
		template_path = "./templates/ServiceHeadless.tmpl"
	}
	//template := template.New("Service")
	template, err := template.ParseFiles(template_path)
	if err != nil {
		log.Error(err)
		return ""
	}
	out := &bytes.Buffer{}
	//err = template.Execute(os.Stdout, service)
	err = template.Execute(out, service)
	if err != nil {
		log.Error(err)
		return ""
	}
	return out.String()
}

func NewServiceMonitorTemplate(service interface{}) string {
	template, err := template.ParseFiles("./templates/ServiceMonitor.tmpl")
	if err != nil {
		log.Error(err)
		return ""
	}
	out := &bytes.Buffer{}
	//err = template.Execute(os.Stdout, service)
	err = template.Execute(out, service)
	if err != nil {
		log.Error(err)
		return ""
	}
	return out.String()
}

func NewEndPointsTemplate(service interface{}) string {
	template, err := template.ParseFiles("./templates/Endpoints.tmpl")
	if err != nil {
		log.Error(err)
		return ""
	}
	out := &bytes.Buffer{}
	//err = template.Execute(os.Stdout, service)
	err = template.Execute(out, service)
	if err != nil {
		log.Error(err)
		return ""
	}
	return out.String()
}
