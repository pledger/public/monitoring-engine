//
// Copyright 2021 Atos
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 02 Nov 2021
// Updated on 02 Nov 2021
//
// @author: ATOS
//
package kafka

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"encoding/pem"
	"io/ioutil"
	"strings"
	"time"

	log "atos.pledger/k2p/common/logs"
	"golang.org/x/crypto/pkcs12"

	//"github.com/confluentinc/confluent-kafka-go/kafka"
	"github.com/segmentio/kafka-go"
	"github.com/spf13/viper"
)

// path used in logs
const pathLOG2 string = "K2P > Kafka > "

// Kafka endpoint server
var KAFKA_ENDPOINT = "localhost:9093"

// Kafka SSL config
var KAFKA_KEYSTORE_LOCATION = "/var/kafka_keystore/kafka.client.keystore.p12"
var KAFKA_KEYSTORE_PASSWORD = ""
var KAFKA_TRUSTSTORE_LOCATION = "/var/kafka_truststore/kafka.client.truststore.pem"

// Connection vars
var dialer *kafka.Dialer = nil

//var reader *kafka.Reader = nil
//var writer *kafka.Writer = nil
var reader map[string]*kafka.Reader
var writer map[string]*kafka.Writer

// init function
func init() {
	log.Println(pathLOG2 + "[init] Initializating Kafka2 client ...")
}

func New2(config *viper.Viper) {
	KAFKA_ENDPOINT = config.GetString("KAFKA_ENDPOINT")
	KAFKA_KEYSTORE_LOCATION = config.GetString("KAFKA_KEYSTORE_LOCATION")
	KAFKA_KEYSTORE_PASSWORD = config.GetString("KAFKA_KEYSTORE_PASSWORD")
	KAFKA_TRUSTSTORE_LOCATION = config.GetString("KAFKA_TRUSTSTORE_LOCATION")
	dialer = getSSLDialer()
	log.Println(pathLOG2 + "Kafka Endpoint: " + KAFKA_ENDPOINT)
	//log.Printf(pathLOG2+"Dialer: %v\n", dialer)
	reader = make(map[string]*kafka.Reader)
	writer = make(map[string]*kafka.Writer)
}

func CheckForMessages2(topic string) (string, error) {
	return consume2(topic)
}

/*
consume Read from Kafka server
*/
func consume2(topic string) (string, error) {
	log.Println(pathLOG2 + "Consume [topic=" + topic + "] ...")
	var msg = ""
	if dialer == nil {
		dialer = getSSLDialer()
	}
	KafkaEndpointList := strings.Split(KAFKA_ENDPOINT, ",")
	//if reader == nil {
	if reader[topic] == nil {
		//reader = kafka.NewReader(kafka.ReaderConfig{
		myreader := kafka.NewReader(kafka.ReaderConfig{
			//Brokers:   []string{KAFKA_ENDPOINT},
			Brokers:   KafkaEndpointList,
			Topic:     topic,
			Partition: 0,
			MinBytes:  10e3, // 10KB
			MaxBytes:  10e6, // 10MB
			Dialer:    dialer,
		})
		reader[topic] = myreader
		reader[topic].SetOffsetAt(context.Background(), time.Now())
	}
	//r.SetOffset(42)
	//for {
	//m, err := reader.ReadMessage(context.Background())
	m, err := reader[topic].ReadMessage(context.Background())
	if err != nil {
		log.Error(pathLOG2+"Error reading message: ", err)
		//break
	}
	//fmt.Printf("message at offset %d: %s = %s\n", m.Offset, string(m.Key), string(m.Value))
	msg = string(m.Value)
	//}
	//if err := r.Close(); err != nil {
	//	log.Error(pathLOG2+"failed to close reader:", err)
	//}
	//conn.Close()
	return msg, err
}

/*
produce Write to Kafka sever
*/
func produce2(topic string, message string) (string, error) {
	log.Println(pathLOG2 + "Produce [topic=" + topic + "] ...")
	if dialer == nil {
		dialer = getSSLDialer()
	}
	KafkaEndpointList := strings.Split(KAFKA_ENDPOINT, ",")
	//if writer == nil {
	if writer[topic] == nil {
		//writer = kafka.NewWriter(kafka.WriterConfig{
		mywriter := kafka.NewWriter(kafka.WriterConfig{
			//Brokers:  []string{KAFKA_ENDPOINT},
			Brokers:  KafkaEndpointList,
			Topic:    topic,
			Balancer: &kafka.Hash{},
			Dialer:   dialer,
		})
		writer[topic] = mywriter
	}
	//w := &kafka.Writer{
	//	Addr:     kafka.TCP(KafkaEndpoint),
	//	Topic:    topic,
	//	Balancer: &kafka.LeastBytes{},
	//}
	//err := writer.WriteMessages(context.Background(),
	err := writer[topic].WriteMessages(context.Background(),
		kafka.Message{
			Topic: topic,
			Value: []byte(message),
		},
	)
	if err != nil {
		log.Error(pathLOG2+"Failed to write messages: ", err)
	}
	//if err := writer.Close(); err != nil {
	if err := writer[topic].Close(); err != nil {
		log.Error(pathLOG2+"Failed to close writer: ", err)
	} else {
		writer = nil
	}
	return "", err
}

func getSSLDialer() *kafka.Dialer {
	log.Println(pathLOG2 + "Get SSLDialer...")
	cert := getCertFromKeyStore(KAFKA_KEYSTORE_LOCATION, KAFKA_KEYSTORE_PASSWORD)
	caCert, err := ioutil.ReadFile(KAFKA_TRUSTSTORE_LOCATION)
	if err != nil {
		log.Error(pathLOG2+"CA cert load:", err)
	}
	caCertPool := x509.NewCertPool()
	caCertPool.AppendCertsFromPEM(caCert)
	dialer := &kafka.Dialer{
		Timeout:   15 * time.Second,
		DualStack: true,
		TLS: &tls.Config{
			Certificates: []tls.Certificate{cert},
			RootCAs:      caCertPool,
			//ClientCAs:                   &x509.CertPool{},
			InsecureSkipVerify: true,
		},
	}
	return dialer
}

func getCertFromKeyStore(keystorage string, password string) tls.Certificate {
	log.Printf(pathLOG2+"Reading cert... %s\n", keystorage)
	pfxdata, err1 := ioutil.ReadFile(keystorage)
	if err1 != nil {
		log.Error(pathLOG2+"Error ReadFile: ", err1)
	}
	blocks, err := pkcs12.ToPEM(pfxdata, password)
	//key, cert, err := pkcs12.Decode(pfxdata, password)
	if err != nil {
		log.Error(pathLOG2+"Error pkcs12.ToPEM: ", err)
	}
	var pemData []byte
	for _, b := range blocks {
		pemData = append(pemData, pem.EncodeToMemory(b)...)
	}
	// then use PEM data for tls to construct tls certificate:
	cert, err2 := tls.X509KeyPair(pemData, pemData)
	if err2 != nil {
		log.Error(pathLOG2+"Error tls.X509KeyPair: ", err2)
	}
	return cert
}
