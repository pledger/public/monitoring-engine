module atos.pledger/k2p

go 1.13

require (
	github.com/oliveagle/jsonpath v0.0.0-20180606110733-2e52cf6e6852
	github.com/prometheus-operator/prometheus-operator/pkg/apis/monitoring v0.44.1
	github.com/prometheus-operator/prometheus-operator/pkg/client v0.45.0
	github.com/prometheus/client_golang v1.11.0
	github.com/prometheus/client_model v0.2.0
	github.com/prometheus/common v0.32.1
	github.com/segmentio/kafka-go v0.4.22
	github.com/sirupsen/logrus v1.8.1
	github.com/spf13/viper v1.9.0
	golang.org/x/crypto v0.0.0-20210921155107-089bfa567519
	golang.org/x/sys v0.0.0-20211103235746-7861aae1554b // indirect
	k8s.io/api v0.19.2
	k8s.io/apimachinery v0.19.5-rc.0
	k8s.io/client-go v0.19.2
)

//replace (
//	github.com/prometheus-operator/prometheus-operator/pkg/apis/monitoring => ./pkg/apis/monitoring
//	github.com/prometheus-operator/prometheus-operator/pkg/client => ./pkg/client
//)
