#!/bin/sh
export OPENAPI_GENERATOR_VERSION=5.3.0
./openapi-generator-cli generate -i openapi.yaml -g go-server -o . --git-host "" --git-repo-id monitoring-engine --git-user-id atos.pledger --additional-properties=serverPort=4040
#-t ./openapi_server/.openapi-generator/templates/python-flask --additional-properties=serverPort=4040
sed -i 's/github.com\///g' ./main.go
sed -i -e 's/\"encoding\/json\"//g' -e 's/\"github.com\/gorilla\/mux\"//g' ./go/api_default.go
#go mod tidy

#cd openapi_server
#docker build -t monitoring-engine .
