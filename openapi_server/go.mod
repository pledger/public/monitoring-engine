module atos.pledger/monitoring-engine

go 1.13

require (
	github.com/gorilla/mux v1.8.0
	github.com/oliveagle/jsonpath v0.0.0-20180606110733-2e52cf6e6852
	github.com/prometheus/client_golang v1.11.0
	github.com/sirupsen/logrus v1.6.0
)
