//
// Copyright 2021 Atos
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 02 Nov 2021
// Updated on 02 Nov 2021
//
// @author: ATOS
//

package openapi

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"regexp"
	"strings"

	"atos.pledger/monitoring-engine/common/logs"
	"atos.pledger/monitoring-engine/e2co"
	"atos.pledger/monitoring-engine/prometheus"
)

// DefaultApiService is a service that implements the logic for the DefaultApiServicer
// This service should implement the business logic for every endpoint for the DefaultApi API.
// Include any external packages or services that will be required by this service.
type DefaultApiService struct {
}

const pathLOG string = "ME > "

// NewDefaultApiService creates a default api service
func NewDefaultApiService() DefaultApiServicer {
	return &DefaultApiService{}
}

// ApiV1QueryGet - Instant queries
func (s *DefaultApiService) ApiV1QueryGet(ctx context.Context, query string, svcId string, time string, timeout int32) (ImplResponse, error) {
	// TODO - update ApiV1QueryGet with the required logic for this service method.
	// Add api_default_service.go to the .openapi-generator-ignore to avoid overwriting this service implementation when updating open api generation.

	//TODO: Uncomment the next line to return response Response(200, []string{}) or use other options such as http.Ok ...
	//return Response(200, []string{}), nil

	logs.Debug("ApiV1QueryGet(" + query + "," + svcId + "," + time + ")")
	metric := extractMetric(query)
	labels := extractLabels(query)
	var endpoint string = e2co.GetPrometheusEndpoint(svcId, metric, labels)
	if len(labels) > 0 {
		query = e2co.FilterbyLabels(query, svcId, metric, labels)
	}
	var result, err = prometheus.ApiQuery(endpoint, query, time, timeout)
	if err != nil {
		//panic(err)
		return Response(http.StatusBadRequest, nil), err
	}

	result = formatPrometheusResult(result)
	//logs.Debug(fmt.Sprintf(pathLOG+"%v", result))

	//return Response(http.StatusNotImplemented, nil), errors.New("ApiV1QueryGet method not implemented")
	return Response(http.StatusOK, result), nil
}

// ApiV1QueryRangeGet - Range queries
func (s *DefaultApiService) ApiV1QueryRangeGet(ctx context.Context, query string, svcId string, start string, end string, step int32, timeout int32) (ImplResponse, error) {
	// TODO - update ApiV1QueryGet with the required logic for this service method.
	// Add api_default_service.go to the .openapi-generator-ignore to avoid overwriting this service implementation when updating open api generation.

	//TODO: Uncomment the next line to return response Response(200, []string{}) or use other options such as http.Ok ...
	//return Response(200, []string{}), nil

	logs.Debug("ApiV1QueryRangeGet(" + query + "," + svcId + "," + start + "," + end + "," + fmt.Sprintf("%d", step) + ")")
	metric := extractMetric(query)
	labels := extractLabels(query)
	var endpoint string = e2co.GetPrometheusEndpoint(svcId, metric, labels)
	var result, err = prometheus.ApiQueryRange(endpoint, query, start, end, step, timeout)
	if err != nil {
		//panic(err)
		return Response(http.StatusBadRequest, nil), err
	}

	result = formatPrometheusResult(result)
	//logs.Debug(fmt.Sprintf(pathLOG+"%v", result))

	//return Response(http.StatusNotImplemented, nil), errors.New("ApiV1QueryGet method not implemented")
	return Response(http.StatusOK, result), nil
}

func extractMetric(promQL string) string {
	var result string
	//re := regexp.MustCompile(`(.*\()?(.*){.*}.*`)
	re := regexp.MustCompile(`^([a-zA-Z0-9_]*\()*([a-zA-Z0-9_]*).*`) // Only extract first metric name ?
	result = re.ReplaceAllString(promQL, `$2`)
	logs.Debug("extractMetric: " + result)
	return result
}

func extractLabels(promQL string) map[string]string {
	var result map[string]string
	//re := regexp.MustCompile(`{.*}`)
	//labels := re.FindString(promQL)
	re := regexp.MustCompile(`{[a-zA-Z0-9_=,]*}`)
	labels := re.FindAllString(promQL, -1)
	logs.Debug("extractLabels: " + fmt.Sprintf("%v", labels))
	if len(labels) > 0 {
		re := regexp.MustCompile(`([a-zA-Z0-9-:.]*)=([a-zA-Z0-9-:.]*)(,|})`)
		for k := range labels {
			//result = re.ReplaceAllString(labels, `"$1":"$2"$3`)
			js := re.ReplaceAllString(labels[k], `"$1":"$2"$3`)
			err := json.Unmarshal([]byte(js), &result)
			if err != nil {
				panic(err)
			}
			break // Only extract first metric labels ?
		}
	}
	return result
}

func formatPrometheusResult(result interface{}) interface{} {
	bytes, err := json.Marshal(result)
	if err != nil {
		panic(err)
	}
	//logs.Debug(string(bytes))
	var resultType string = "vector"
	matched := strings.Contains(string(bytes), "\"values\":[[")
	if matched {
		resultType = "matrix"
	}
	tmp := "{\"status\":\"success\",\"data\":{\"resultType\":\"" + resultType + "\",\"result\":" + string(bytes) + "}}"
	err = json.Unmarshal([]byte(tmp), &result)
	if err != nil {
		panic(err)
	}
	return result
}
