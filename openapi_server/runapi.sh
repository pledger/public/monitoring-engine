#!/bin/sh
export ME_DEFAULT_PROM=http://localhost:9090
export ME_E2CO_ENDPOINT=http://localhost:8000
export ME_E2CO_CACHE_TIMEOUT=60
#cd openapi_server
go mod tidy
go run main.go 

#docker run --rm --name monitoring-engine -p 4040:4040 monitoring-engine
