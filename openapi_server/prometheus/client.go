//
// Copyright 2021 Atos
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//     https://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Created on 02 Nov 2021
// Updated on 02 Nov 2021
//
// @author: ATOS
//
package prometheus

import (
	"context"
	"fmt"
	"strconv"
	"time"

	"atos.pledger/monitoring-engine/common/logs"
	"github.com/prometheus/client_golang/api"
	v1 "github.com/prometheus/client_golang/api/prometheus/v1"
)

func ApiQuery(endpoint string, query string, mytime string, timeout int32) (interface{}, error) {
	//var result string
	client, err := api.NewClient(api.Config{
		Address:      endpoint,
		RoundTripper: nil,
	})
	if err != nil {
		logs.Error("Error creating client: ", err)
		//panic(err)
		return nil, err
	}

	timestamp, _ := parseTimeParam(mytime)

	v1api := v1.NewAPI(client)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	result, warnings, err := v1api.Query(ctx, query, timestamp)
	if err != nil {
		logs.Error("Error querying Prometheus: ", err)
		//panic(err)
		return nil, err
	}
	if len(warnings) > 0 {
		logs.Warn("Warnings: ", warnings)
	}
	//logs.Debug(fmt.Sprintf("Result:\n%v\n", result))
	return result, nil
}

func ApiQueryRange(endpoint string, query string, start string, end string, step int32, timeout int32) (interface{}, error) {
	client, err := api.NewClient(api.Config{
		Address:      endpoint,
		RoundTripper: nil,
	})
	if err != nil {
		logs.Error("Error creating client: ", err)
		//panic(err)
		return nil, err
	}

	v1api := v1.NewAPI(client)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	//timeStart := time.Now().Add(-time.Hour)
	//timeEnd := time.Now()
	//timeStep := time.Minute
	timeStart, _ := parseTimeParam(start)
	timeEnd, _ := parseTimeParam(end)
	timeStep := time.Duration(step) * time.Second
	r := v1.Range{
		Start: timeStart,
		End:   timeEnd,
		Step:  timeStep,
	}
	result, warnings, err := v1api.QueryRange(ctx, query, r)
	if err != nil {
		logs.Error("Error querying Prometheus: ", err)
		//panic(err)
		return nil, err
	}
	if len(warnings) > 0 {
		logs.Warn("Warnings: ", warnings)
	}
	//logs.Debug(fmt.Sprintf("Result:\n%v\n", result))
	return result, nil
}

func parseTimeParam(mytime string) (time.Time, error) {
	var timestamp time.Time = time.Now()
	var err error
	if len(mytime) > 0 {
		i, err := strconv.ParseInt(mytime, 10, 64)
		if err == nil {
			timestamp = time.Unix(i, 0)
		} else {
			timestamp, err = time.Parse(time.RFC3339, mytime)
			if err != nil {
				timestamp = time.Now()
			}
		}
	}
	return timestamp, err
}

func ApiMetadata(endpoint string, metric string) ([]string, error) {
	//logs.Debug(fmt.Sprintln("ApiMetadata(" + endpoint + "," + metric + ")"))
	if metric == "up" {
		return []string{"up"}, nil
	}
	client, err := api.NewClient(api.Config{
		Address:      endpoint,
		RoundTripper: nil,
	})
	if err != nil {
		logs.Error("Error creating client: ", err)
		//panic(err)
		return nil, err
	}
	v1api := v1.NewAPI(client)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	metadata, err := v1api.Metadata(ctx, metric, "1")
	if err != nil {
		logs.Error("Error querying Prometheus: ", err)
		//panic(err)
		return nil, err
	}
	logs.Debug(fmt.Sprintf("Metric Metadata: %v", metadata))
	result := make([]string, 0)
	for k := range metadata {
		result = append(result, k)
	}
	return result, nil
}
